package xin.cymall.service;


import xin.cymall.entity.User;

import java.util.List;
import java.util.Map;

/**
 * 用户
 * 
 * @author chenyi
 * @email 228112142@qq.com
 * @date 2017-03-23 15:22:06
 */
public interface UserService {

	User queryObject(String userId);
	
	List<User> queryList(Map<String, Object> map);
	
	int queryTotal(Map<String, Object> map);
	
	void save(String username,String mobile, String password);
	
	void update(User user);
	
	void delete(Long userId);
	
	void deleteBatch(Long[] userIds);


	/**
	 * 用户登录
	 * @param username  用户名
	 * @param password  密码
	 * @return          返回用户ID
	 */
	String login(String username, String password);
}
